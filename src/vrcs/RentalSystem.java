package vrcs;

import cost.CostException;
import customer.AbstractCustomer;
import customer.Company;
import customer.Person;
import exception.ModelNotFoundException;
import exception.RentCheckException;
import manager.CustomerManager;
import manager.VehicleManager;
import menu.Shell;
import type.EnumCustomerType;
import type.EnumVehicleType;
import vehicle.AbstractVehicle;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;

@SuppressWarnings("unused")
public class RentalSystem {
    private VehicleManager vehicleManager;
    private CustomerManager customerManager;

    public RentalSystem() {
        vehicleManager = new VehicleManager();
        customerManager = new CustomerManager();
    }

    public void callMethod(String methodName) {
        try {
            Method method = this.getClass().getMethod(methodName);
            method.invoke(this);
        } catch (SecurityException | NoSuchMethodException |
                IllegalAccessException | IllegalArgumentException | InvocationTargetException e) {
            e.printStackTrace();
        }
    }

    public String getMethodName(String option) {
        String methodName = option.split("[.]")[option.split("[.]").length - 1]
                .replaceAll("\\s", "").trim();
        return Character.toLowerCase(methodName.charAt(0)) + methodName.substring(1);
    }

    public void listAllVehicles() {
        for (AbstractVehicle vehicle: vehicleManager.listAll()) {
            System.out.printf("%-10s %-10d %-10s\n", vehicle.getType(), vehicle.getId(), vehicle.getNumberPlate());
        }
    }

    public void listVehiclesForPerson() {
        for (AbstractVehicle vehicle: vehicleManager.listAll()) {
            if (vehicle.vehicleClass().equals(EnumVehicleType.PERSONAL))
                System.out.printf("%-10s %-10d %-10s\n", vehicle.getType(), vehicle.getId(), vehicle.getNumberPlate());
        }
    }

    public void listVehiclesForCompany() {
        for (AbstractVehicle vehicle: vehicleManager.listAll()) {
            if (vehicle.vehicleClass().equals(EnumVehicleType.TRADE))
                System.out.printf("%-10s %-10d %-10s\n", vehicle.getType(), vehicle.getId(), vehicle.getNumberPlate());
        }
    }

    public void listCostOfAllVehicles() {
        for (AbstractVehicle vehicle: vehicleManager.listAll()) {
            System.out.printf("%-10s %-10d %-10.1f\n", vehicle.getType(), vehicle.getId(), vehicle.getBasePrice());
        }
    }

    public void listCostOfVehiclesAvailableForPerson() {
        for (Person person: getPeople()) {
            System.out.printf("Vehicles that %s (%d) can buy:\n", person.getName(), person.getCustomerId());
            for (AbstractVehicle vehicle: vehicleManager.listAll()) {
                if (vehicle.vehicleClass().equals(EnumVehicleType.PERSONAL))
                    System.out.printf("%-10s %-10d %-10.1f\n", vehicle.getType(),
                            vehicle.getId(), vehicle.getBasePrice());
            }
        }
    }

    public void listCostOfVehiclesAvailableForCompany() {
        for (Company company: getCompanies()) {
            System.out.printf("Vehicles that %s (%d) can buy:\n", company.getName(), company.getCustomerId());
            for (AbstractVehicle vehicle: vehicleManager.listAll()) {
                if (vehicle.vehicleClass().equals(EnumVehicleType.TRADE))
                    System.out.printf("%-10s %-10d %-10.1f\n", vehicle.getType(),
                            vehicle.getId(), vehicle.getBasePrice());
            }
        }
    }

    public void rentAVehicle() {
        try {
            AbstractCustomer customer = customerManager.byId(Integer.parseInt(Shell.getBasicInput("Enter your customer id: ")));
            AbstractVehicle vehicle = vehicleManager.byId(
                    Integer.parseInt(Shell.getBasicInput("Enter the id of the vehicle you want to rent: ")));
            System.out.printf("%-10s %-10d\n", vehicle.getType(), vehicle.getId());
            customer.canRent(vehicle);
            System.out.printf("Rent is successful, the cost of the vehicle for one day is %.1f\n", vehicle.calculateCost());
        } catch (ModelNotFoundException | CostException e) {
            e.printStackTrace();
        } catch (RentCheckException e) {
            System.err.println("Rent is failed. Error : " + e.getMessage());
        }
    }

    public void addVehicle(AbstractVehicle vehicle) {
        vehicleManager.add(vehicle.getId(), vehicle);
    }

    public void addCustomer(AbstractCustomer abstractCustomer) {
        customerManager.add(abstractCustomer.getCustomerId(), abstractCustomer);
    }

    private ArrayList<Person> getPeople() {
        ArrayList<Person> people = new ArrayList<>();
        for (AbstractCustomer abstractCustomer : customerManager.listAll()) {
            if (abstractCustomer.customerType().equals(EnumCustomerType.PERSON))
                people.add((Person) abstractCustomer);
        }
        return people;
    }

    private ArrayList<Company> getCompanies() {
        ArrayList<Company> companies = new ArrayList<>();
        for (AbstractCustomer abstractCustomer : customerManager.listAll()) {
            if (abstractCustomer.customerType().equals(EnumCustomerType.COMPANY))
                companies.add((Company) abstractCustomer);
        }
        return companies;
    }
}

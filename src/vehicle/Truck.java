package vehicle;

import cost.CostException;
import type.EnumDrivingLicense;
import type.EnumFuelType;

public class Truck extends Carrier {

	private double tonnageCapacity;
	public Truck(int id, String type, String numberPlate, double basePrice,
                 EnumFuelType fuelType, double tonnageCapacity) {
		super(id, type, numberPlate, basePrice, fuelType);
		this.setTonnageCapacity(tonnageCapacity);
	}

	@Override
	public double calculateCost() throws CostException {
		if(getBasePrice() < 0 || getTonnageCapacity() <= 0) {
			throw new CostException("Cost parameter must be greater than 0");
		}
		return getBasePrice() + (getTonnageCapacity() * 100) +
				(getFuelType() == EnumFuelType.BENZINE ? 30 : 40);
	}

	private double getTonnageCapacity() {
		return tonnageCapacity;
	}
	
	private void setTonnageCapacity(double tonnageCapacity) {
		this.tonnageCapacity = tonnageCapacity;
	}

	@Override
	public EnumDrivingLicense requiredLicense() { return EnumDrivingLicense.A2; }
}

package vehicle;

import type.EnumDrivingLicense;
import type.EnumVehicleType;

public interface Vehicle {

    EnumDrivingLicense requiredLicense();

    EnumVehicleType vehicleClass();
}

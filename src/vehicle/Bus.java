package vehicle;

import cost.CostException;
import type.EnumDrivingLicense;
import type.EnumFuelType;

public class Bus extends Carrier {

	private int seatCapacity;

	public Bus(int id, String type, String numberPlate, double basePrice,
               EnumFuelType fuelType, int seatCapacity) {
		super(id, type, numberPlate, basePrice, fuelType);
		this.setSeatCapacity(seatCapacity);
	}

	@Override
	public double calculateCost() throws CostException {
		if(getBasePrice() < 0 || getSeatCapacity() <= 0) {
			throw new CostException("Cost parameter must be greater than 0");
		}
		return getBasePrice() + (getSeatCapacity() * 10) +
				(getFuelType() == EnumFuelType.BENZINE ? 30 : 40);
	}

	private int getSeatCapacity() {
		return seatCapacity;
	}

	private void setSeatCapacity(int seatCapacity) {
		this.seatCapacity = seatCapacity;
	}

	@Override
	public EnumDrivingLicense requiredLicense() { return EnumDrivingLicense.A2; }
}

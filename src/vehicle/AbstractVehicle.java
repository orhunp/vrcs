package vehicle;

import cost.Cost;

public abstract class AbstractVehicle implements Cost, Vehicle {
	
	private int id;
	private String type, numberPlate;
	private double basePrice;

	AbstractVehicle(int id, String type, String numberPlate, double basePrice) {
		this.setId(id);
		this.setType(type);
		this.setNumberPlate(numberPlate);
		this.setBasePrice(basePrice);
	}

	public int getId() {
		return id;
	}

	private void setId(int id) {
		this.id = id;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}
	
	public String getNumberPlate() {
		return numberPlate;
	}

	private void setNumberPlate(String numberPlate) {
		this.numberPlate = numberPlate;
	}

	public double getBasePrice() {
		return basePrice;
	}
	
	private void setBasePrice(double basePrice) {
		this.basePrice = basePrice;
	}
}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
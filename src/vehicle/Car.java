package vehicle;

import cost.CostException;
import type.EnumDrivingLicense;
import type.EnumGearType;
import type.EnumVehicleType;

public class Car extends AbstractVehicle {

	private boolean airBag;
	private EnumGearType gearType;
	private int age;

	public Car(int id, String type, String numberPlate, double basePrice,
			   boolean airBag, EnumGearType gearType, int age) {
		super(id, type, numberPlate, basePrice);
		this.setAirBag(airBag);
		this.setGearType(gearType);
		this.setAge(age);
	}
	
	@Override
	public double calculateCost() throws CostException {
		if(getBasePrice() < 0 || getAge() <= 0) {
			throw new CostException("Cost parameter must be greater than 0");
		}
	    return getBasePrice() + (getAirBag() ? 100 : 0) +
	        (getGearType() == EnumGearType.AUTOMATIC ? 200 : 0) +
	        (getAge() < 5 ? 50 : 0);
	}

	private boolean getAirBag() {
		return airBag;
	}
	
	private void setAirBag(boolean airBag) {
		this.airBag = airBag;
	}

	private EnumGearType getGearType() {
		return gearType;
	}

	private void setGearType(EnumGearType gearType) {
		this.gearType = gearType;
	}

	private int getAge() {
		return age;
	}

	private void setAge(int age) {
		this.age = age;
	}

	@Override
	public EnumDrivingLicense requiredLicense() { return EnumDrivingLicense.B; }

	@Override
	public EnumVehicleType vehicleClass() { return EnumVehicleType.PERSONAL; }
}

package vehicle;

import type.EnumFuelType;
import type.EnumVehicleType;

abstract class Carrier extends AbstractVehicle {

	private EnumFuelType fuelType;

	Carrier(int id, String type, String numberPlate, double basePrice, EnumFuelType fuelType) {
		super(id, type, numberPlate, basePrice);
		this.setFuelType(fuelType);
	}

	EnumFuelType getFuelType() {
		return fuelType;
	}

	private void setFuelType(EnumFuelType fuelType) {
		this.fuelType = fuelType;
	}

	@Override
	public final EnumVehicleType vehicleClass() { return EnumVehicleType.TRADE; }
}

package vehicle;

import cost.CostException;
import type.EnumDrivingLicense;
import type.EnumMotorType;
import type.EnumVehicleType;

public class Motor extends AbstractVehicle {

private EnumMotorType motorType;
	
	public Motor(int id, String type, String numberPlate, double basePrice, EnumMotorType motorType) {
		super(id, type, numberPlate, basePrice);
		this.setMotorType(motorType);
	}

	@Override
	public double calculateCost() throws CostException {
		if(getBasePrice() < 0) {
			throw new CostException("Cost parameter must be greater than 0");
		}
		double cost = getBasePrice();
		switch (getMotorType()) {
		case SCOOTER:
			cost += 50;
			break;
		case MOTORBIKE:
			cost += 100;
			break;
		}
		return cost;
	}

	private EnumMotorType getMotorType() {
		return motorType;
	}

	private void setMotorType(EnumMotorType motorType) {
		this.motorType = motorType;
	}

	@Override
	public EnumDrivingLicense requiredLicense() { return EnumDrivingLicense.A2; }

	@Override
	public EnumVehicleType vehicleClass() { return EnumVehicleType.PERSONAL; }
}

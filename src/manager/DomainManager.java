package manager;

import exception.ModelNotFoundException;

import java.util.List;

public interface DomainManager<T> {
    T byId(int id) throws ModelNotFoundException;
    List<T> listAll();
    void add(int id, T model);
    void remove(int id);
}

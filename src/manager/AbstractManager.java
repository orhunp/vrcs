package manager;

import exception.ModelNotFoundException;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class AbstractManager<T>  implements DomainManager<T> {

    private Map<Integer, T> modelMap = new HashMap<>();

    @Override
    public T byId(int id) throws ModelNotFoundException {
        T model = modelMap.get(id);
        if(model == null)
            throw new ModelNotFoundException(id, "Model not found");
        return model;
    }

    @Override
    public List<T> listAll() {
        return new ArrayList<>(modelMap.values());
    }

    @Override
    public void add(int id, T model) {
        modelMap.put(id, model);
    }

    @Override
    public void remove(int id) {
        modelMap.remove(id);
    }
}

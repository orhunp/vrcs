package exception;

public class ModelNotFoundException extends Exception {
    private int id;
    private String message;

    public ModelNotFoundException(int id, String message) {
        super(message);
        this.id = id;
        this.message = message;
    }

    public int getId() {
        return id;
    }

    @Override
    public String getMessage() {
        return message;
    }
}

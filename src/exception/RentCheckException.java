package exception;

public class RentCheckException extends Exception{

    private String message;

    public RentCheckException(String message) {
        super(message);
        this.message = message;
    }

    public String getMessage() { return message; }
}

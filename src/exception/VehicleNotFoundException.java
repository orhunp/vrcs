package exception;

public class VehicleNotFoundException extends Exception {
    private String message;

    public VehicleNotFoundException(String message) {
        super(message);
        this.message = message;
    }

    public String getMessage() {
        return message;
    }
}

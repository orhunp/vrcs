package main;

import menu.Option;
import menu.Shell;
import customer.Company;
import customer.Person;
import vrcs.RentalSystem;
import type.EnumDrivingLicense;
import type.EnumFuelType;
import type.EnumGearType;
import type.EnumMotorType;
import vehicle.Bus;
import vehicle.Car;
import vehicle.Motor;
import vehicle.Truck;

public class Main {

    private static RentalSystem rentalSystem;

    public static void main(String[] args) {
        rentalSystem = new RentalSystem();
        loadDummyValues();
        Shell shell = new Shell("Welcome to the Vehicle Rental Corporation!",
                new Option[] {
                        new Option("1. Guest Entrance", new Option[] {
                                new Option("1.1. List All Vehicles"),
                                new Option("1.2. List Vehicles For Person"),
                                new Option("1.3. List Vehicles For Company"),
                                new Option("1.4. List Cost Of All Vehicles")

                        }),
                        new Option("2. Person Entrance", new Option[] {
                                new Option("2.1. List Cost Of Vehicles Available For Person"),
                                new Option("2.2. Rent A Vehicle"),
                        }),
                        new Option("3. Company Entrance", new Option[] {
                                new Option("3.1. List Cost Of Vehicles Available For Company"),
                                new Option("3.2. Rent A Vehicle"),
                        })
                },
                option -> rentalSystem.callMethod(rentalSystem.getMethodName(option.getText()))
        );
        while (shell.isRunning()) {
            switch (shell.nextInput()) {
                case "ask":
                    System.out.print("Present Menu(P)/Main Menu(M)/Exit(E): ");
                    break;
                case "list":
                    shell.getOptionHandler().printOptions();
                    break;
                case "exit":
                    shell.exit();
                    break;
                default:
                    System.out.println("Unrecognized command.");
                    shell.getOptionHandler().printOptions();
                    break;
            }
        }
    }

    private static void loadDummyValues() {
        rentalSystem.addVehicle(new Car(101, "Car", "06AB1212", 100, true, EnumGearType.AUTOMATIC, 3));
        rentalSystem.addVehicle(new Car(102, "Car", "06BG0101", 200, true, EnumGearType.MANUAL, 2));
        rentalSystem.addVehicle(new Car(103, "Car", "06AD2324", 100, false, EnumGearType.MANUAL, 6));
        rentalSystem.addVehicle(new Motor(201, "Motor", "34AA123", 40, EnumMotorType.SCOOTER));
        rentalSystem.addVehicle(new Motor(202, "Motor", "34BB123", 60, EnumMotorType.MOTORBIKE));
        rentalSystem.addVehicle(new Motor(203, "Motor", "34CC123", 20, EnumMotorType.SCOOTER));
        rentalSystem.addVehicle(new Truck(301, "Truck", "78AA321", 60, EnumFuelType.BENZINE, 10));
        rentalSystem.addVehicle(new Truck(302, "Truck", "78BB321", 80, EnumFuelType.DIESEL, 20));
        rentalSystem.addVehicle(new Truck(303, "Truck", "78CC321", 90, EnumFuelType.DIESEL, 30));
        rentalSystem.addVehicle(new Bus(401, "Bus", "25SS432", 100, EnumFuelType.BENZINE, 35));
        rentalSystem.addVehicle(new Bus(402, "Bus", "25VV432", 150, EnumFuelType.BENZINE, 40));
        rentalSystem.addVehicle(new Bus(403, "Bus", "25TT432", 200, EnumFuelType.DIESEL, 45));
        rentalSystem.addCustomer(new Person(11, "BerilErtan", EnumDrivingLicense.B));
        rentalSystem.addCustomer(new Person(12, "BerraCan", EnumDrivingLicense.B));
        rentalSystem.addCustomer(new Person(13, "DilaraUzun", EnumDrivingLicense.A2));
        rentalSystem.addCustomer(new Company(1001, "Opet", 1992));
        rentalSystem.addCustomer(new Company(1002, "Eti", 1983));
        rentalSystem.addCustomer(new Company(1003, "Gezitur", 1999));
    }
}
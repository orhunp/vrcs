package menu;

import java.util.Stack;

public class OptionHandler {
    private Option[] options;
    private Stack<Option[]> optionStack;

    OptionHandler(Option[] options) {
        this.options = options;
        optionStack = new Stack<>();
    }

    Option[] getOptions() {
        return options;
    }

    Stack<Option[]> getOptionStack() {
        return optionStack;
    }

    public void printOptions() {
        for (Option option: options) {
            System.out.println(option.getText());
        }
        System.out.println(optionStack.isEmpty() ? "0. Exit" : "0. Back");
        System.out.print("> ");
    }

    boolean selectNext(int index, OptionReceiver optionReceiver) {
        if (options[index - 1].getSubOptions().length == 0) {
            optionReceiver.optionReceived(options[index - 1]);
            return true;
        } else {
            optionStack.push(options);
            options = options[index - 1].getSubOptions();
            return false;
        }
    }

    void selectPrevious() {
        options = optionStack.lastElement();
        optionStack.pop();
    }

    void selectFirst() {
        if (optionStack.size() > 0) {
            options = optionStack.peek();
            optionStack.clear();
        }
    }
}

package menu;

public interface OptionReceiver {
    void optionReceived(Option option);
}

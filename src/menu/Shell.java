package menu;

import java.util.Scanner;

public class Shell {
    private boolean running;
    private Scanner scanner;
    private OptionHandler optionHandler;
    private OptionReceiver optionReceiver;

    public Shell(String startMsg, Option[] options, OptionReceiver optionReceiver) {
        System.out.println(startMsg);
        this.optionReceiver = optionReceiver;
        optionHandler = new OptionHandler(options);
        optionHandler.printOptions();
        scanner = new Scanner(System.in);
        running = true;
    }

    public static String getBasicInput(String message) {
        System.out.print(message);
        Scanner scanner = new Scanner(System.in);
        return scanner.nextLine();
    }

    public OptionHandler getOptionHandler() {
        return optionHandler;
    }

    public boolean isRunning() {
        return running;
    }

    public void exit() {
        running = false;
        scanner.close();
    }

    private String processNumericInput(String input) {
        try {
            int index = Integer.valueOf(input);
            if (index == 0 && !optionHandler.getOptionStack().isEmpty()) {
                optionHandler.selectPrevious();
                return "list";
            } else if (index > 0 && index <= optionHandler.getOptions().length) {
                return optionHandler.selectNext(index, optionReceiver) ? "ask" : "list";
            } else if (index == 0) {
                return "exit";
            }
        } catch (NumberFormatException e) { /* ignore conversion error */ }
        return input;
    }

    public String nextInput() {
        String input = scanner.nextLine().toLowerCase();
        switch (input) {
            case "p":
                return "list";
            case "m":
                optionHandler.selectFirst();
                return "list";
            case "e":
                return "exit";
                default:
                    return processNumericInput(input);
        }
    }
}

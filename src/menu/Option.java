package menu;

public class Option {
    private String text;
    private Option[] subOptions;

    public Option(String text, Option subOptions[]) {
        this.text = text;
        this.subOptions = subOptions;
    }

    public Option(String text) {
        this.text = text;
        subOptions = new Option[] {};
    }

    public String getText() {
        return text;
    }

    Option[] getSubOptions() {
        return subOptions;
    }
}

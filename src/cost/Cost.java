package cost;

public interface Cost {

	double calculateCost() throws CostException;
}
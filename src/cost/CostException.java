package cost;

public class CostException extends Exception {
private String message;

	public CostException(String message) {
		super(message);
		this.message = message;
	}

	public String getMessage() {
		return message;
	}
}

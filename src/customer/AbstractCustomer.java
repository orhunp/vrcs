package customer;

public abstract class AbstractCustomer implements Customer, VehicleRenter {

	private int customerId;

	private String name;
	
	AbstractCustomer(int customerId, String name) {
		this.setCustomerId(customerId);
		this.setName(name);
	}

	public int getCustomerId() {
		return customerId;
	}

	private void setCustomerId(int customerId) {
		this.customerId = customerId;
	}

	public String getName() {
		return name;
	}

	private void setName(String name) {
		this.name = name;
	}

}

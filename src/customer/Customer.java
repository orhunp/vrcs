package customer;

import type.EnumCustomerType;

public interface Customer {

    EnumCustomerType customerType();

    String getName();

}

package customer;

import exception.RentCheckException;
import type.EnumCustomerType;
import type.EnumDrivingLicense;
import type.EnumVehicleType;
import vehicle.Vehicle;

public class Person extends AbstractCustomer {

	private EnumDrivingLicense drivingLicense;
	
	public Person(int customerId, String name, EnumDrivingLicense drivingLicense) {
		super(customerId, name);
		this.setDrivingLicense(drivingLicense);
	}

	@SuppressWarnings("unused")
	public EnumDrivingLicense getDrivingLicense() {
		return drivingLicense;
	}

	private void setDrivingLicense(EnumDrivingLicense drivingLicense) {
		this.drivingLicense = drivingLicense;
	}

	@Override
	public void canRent(Vehicle vehicle) throws RentCheckException {
		if(!vehicle.vehicleClass().equals(EnumVehicleType.PERSONAL))
			throw new RentCheckException("Person " + getName() + " can only personal class vehicles");

		if(!vehicle.requiredLicense().equals(drivingLicense))
			throw new RentCheckException("Person " + getName() + " cannot rent " + vehicle.toString() +
					" Required Licence : " + vehicle.requiredLicense());
	}

	@Override
	public EnumCustomerType customerType() { return EnumCustomerType.PERSON; }
}
package customer;

import exception.RentCheckException;
import type.EnumCustomerType;
import type.EnumVehicleType;
import vehicle.Vehicle;

public class Company extends AbstractCustomer {
	private int yearOfIncorporation;
	
	public Company(int customerId, String name, int yearOfIncorporation) {
		super(customerId, name);
		this.setYearOfIncorporation(yearOfIncorporation);
	}

	@SuppressWarnings("unused")
	public int getYearOfIncorporation() {
		return yearOfIncorporation;
	}

	private void setYearOfIncorporation(int yearOfIncorporation) {
		this.yearOfIncorporation = yearOfIncorporation;
	}

	@Override
	public void canRent(Vehicle vehicle) throws RentCheckException {
		if(!vehicle.vehicleClass().equals(EnumVehicleType.TRADE))
			throw new RentCheckException("Companies can rent only trade class vehicles");
	}

	@Override
	public EnumCustomerType customerType() { return EnumCustomerType.COMPANY; }
}
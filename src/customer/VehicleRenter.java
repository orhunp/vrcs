package customer;

import exception.RentCheckException;
import vehicle.Vehicle;

interface VehicleRenter {

	void canRent(Vehicle vehicle) throws RentCheckException;
}